ARG UBUNTU_VERSION=20.04

FROM ubuntu:${UBUNTU_VERSION}

ARG RUBY_VERSION=3.0.2
ARG BUNDLER_VERSION=2.3.4

MAINTAINER developers@clustermarket.com

ENV TZ=UTC
ENV LANG C.UTF-8
ENV LANGUAGE C:en
ENV LC_ALL C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive
ENV UID=1000
ENV GID=1000

ARG RBENV_ROOT=/home/app/.rbenv

SHELL ["/bin/sh", "-c"]

RUN ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime &&  \
    echo ${TZ} > /etc/timezone && \
    apt-get update && \
    apt-get install -y \
      build-essential \
      curl \
      git \
      libssl-dev \
      libcurl4-openssl-dev \
      libxml2-dev \
      libncurses5-dev \
      libreadline6-dev \
      libyaml-dev \
      zlib1g-dev \
      libjemalloc-dev && \
    apt-get clean && \
    useradd -m -s /bin/sh --uid ${UID} app && \
    echo 'eval "$(rbenv init -)"' > /etc/profile.d/rbenv.sh

USER ${GID}:${UID}

ENV PATH=${RBENV_ROOT}/bin:${RBENV_ROOT}/shims:$PATH

RUN git clone https://github.com/rbenv/rbenv.git ${RBENV_ROOT} && \
    git clone https://github.com/rbenv/ruby-build.git ${RBENV_ROOT}/plugins/ruby-build && \
    PREFIX=${RBENV_ROOT} ${RBENV_ROOT}/plugins/ruby-build/install.sh && \
    RUBY_CONFIGURE_OPTS='--with-jemalloc' rbenv install ${RUBY_VERSION} --skip-existing && \
    rbenv global ${RUBY_VERSION} && \
    gem install bundler -v $BUNDLER_VERSION


ENV LD_PRELOAD=$LD_PRELOAD:/lib/x86_64-linux-gnu/libjemalloc.so.2
